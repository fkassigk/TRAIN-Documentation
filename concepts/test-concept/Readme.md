Purpose

The purpose of this Testconcept is to describe the general Testmethods and -strategies of the Identity Management and Trust Subcomponent “Trust Management Infrastructure for Gaia-X (TRAIN)”

Introduction

The trust management infrastructure enables the establishment of a root of trust for entities acting in the Gaia-X ecosystem and credentials issued by these entities. This is achieved through the introduction of trust lists combined with anchoring of pointers in the DNS following the TRAIN (Trust Management Infrastructure) concept. These lists, published by Governance Authorities, include entities that are certified according to a certain Trust Framework that is maintained by the respective governance authority. This, for example, supports verifying entities in examining the trustworthiness of Issuers through inclusion in trust lists under a specific trust framework that is administered by a specific governance authority.
Gaia-X Federations and other entities are supported in the sovereign publication and administration of trust lists for specific trust frameworks.

Components

- Trust Framework Configuration
- Trust List Management
- Zone Manager Handler
- Trusted Content Resolver
- DNS Zone Manager

Functions

1. Provision of a Trust Framework and Trust lists (TSPA Manager is responsible for this functionality):
- Allows for configuration of a Trust Framework
- Allows for Trust List Management
- Provides federation/organization/participant specific Trust Lists in different formats
2. Anchoring a Trust Framework and Trust List into the DNS (Zone Manager is responsible for this functionality):
- Allows for global discovery based on an established and trusted infrastructure
- Trust Frameworks are anchored in DNS Pointer Resource Record (PTR RR)
- Trust List URI DID is anchored in DNS URI Resource Record (URI RR)
- DNSSEC allows for chain of trust
3. Enrollment of trusted entities into the Trust Framework (TSPA connector is responsible for this functionality):
- Notary (via the Notarization API) uses the TSPA connector to enroll trusted entities to the Trust Lists
4. Verifying the Institutional Trust of Verifiable Credentials:
- Trusted Content Resolver is responsible for this functionality
- Allows Global Discovery of Trust Frameworks through DNS Resolver
- The content from terms of Use of the verifiable credential will be used for trust discovery
- Verification of issuer details of the credential with the information of the trust list
- Verification of the Integrity of VC
- Validation of the Integrity of the chain of trust of DNSSEC

Software Quality Attributes

- Technical Testing on different layers (unit, component, integration)
- E2E-Tests for functional requirements using automated BDD methodology
- Branch Model
- Patch Management
- Code Quality Verification
- Performance Tests for Up/Down Scale

Description of the automatic E2E-Tests

1. The acceptance criteria of the functional requirements based on Software Requirements Specification for IDM.TRAIN-Document will be selected and transformed to testcases in BDD-Syntax, i.e. to tests written in a natural language style
2. The BDD testcases will be integrated with Python behave to automatic Testscripts
3. Gitlab is responsible for the Jenkins Pipeline
4. Jenkins sets up the Testing Environment and creates Status Reports
